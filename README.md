# README #

Use-cases:

0) Print help:
```
java -jar idsearcher.jar --help
```

1) Save proxy settings in {USER_HOME}/idsearch.properties (default properties file)
```
java -jar idsearcher.jar --proxy-name <USERNAME> --proxy-password <PASSWORD> --proxy-host <HOST> --proxy-port <PORT> --save
```

2) Save url pattern in specified file
```
java -jar idsearcher.jar --url-pattern http://www.ulmart.ru/search?string={?}&rootCategory=&sort=6 --save SEARCH_BY_NAME.properties
```

3) Load url pattern from file and set arguments
```
java -jar idsearcher.jar --pattern-arguments apple --load SEARCH_BY_NAME.properties
```