package ru.ulmart.util.idsearcher;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 *
 * <p>
 * @author Artem Labazin, xxlabaza@gmail.com
 * <p>
 * @since Aug 20, 2014
 * <p>
 * @version 1.0.0
 */
public class Settings {

    private static Settings instance;

    public static Settings getInstance () {
        if (instance == null) {
            instance = new Settings();
        }
        return instance;
    }

    public static Settings getInstance (String[] args) {
        if (instance == null) {
            instance = new Settings();
        }
        instance.parse(args);
        return instance;
    }

    private final Options options;

    private final Path propertiesFile;

    private final CommandLine homeCmd;

    private CommandLine runtimeCmd;

    private CommandLine loadCmd;

    private Settings () {
        propertiesFile = Paths.get(System.getProperty("user.home") + "/idsearch.properties");

        options = new Options();

        options.addOption("h", "help", false, "print this help");
        options.addOption("v", "verbose", false, "print product IDs with product names");
        options.addOption(OptionBuilder.withLongOpt("save")
                .hasOptionalArg()
                .withArgName("FILE_NAME")
                .withDescription("dry run, save setted options to $HOME/idsearch.properties " +
                                 "or, if FILE_NAME was setted, to specified file")
                .create('s'));
        options.addOption(OptionBuilder.withLongOpt("load")
                .hasArg()
                .withArgName("FILE_NAME")
                .withDescription("use options from specified FILE_NAME")
                .create('l'));

        options.addOption(OptionBuilder.withLongOpt("url-pattern")
                .hasArg()
                .withArgName("URL_PATTERN")
                .withDescription("url pattern")
                .create('u'));
        options.addOption(OptionBuilder.withLongOpt("pattern-arguments")
                .hasArgs(Option.UNLIMITED_VALUES)
                .withValueSeparator(',')
                .withArgName("PATTERN_ARGUMENTS")
                .withDescription("pattern arguments")
                .create('a'));

        options.addOption(OptionBuilder.withLongOpt("proxy-name")
                .hasArg()
                .withArgName("USER_NAME")
                .withDescription("proxy user name")
                .create());
        options.addOption(OptionBuilder.withLongOpt("proxy-password")
                .hasArg()
                .withArgName("USER_PASSWORD")
                .withDescription("proxy password")
                .create());
        options.addOption(OptionBuilder.withLongOpt("proxy-host")
                .hasArg()
                .withArgName("PROXY_HOST")
                .withDescription("proxy host")
                .create());
        options.addOption(OptionBuilder.withLongOpt("proxy-port")
                .hasArg()
                .withArgName("PROXY_PORT")
                .withDescription("proxy port")
                .create());

        String[] arguments = getStoredOptions(propertiesFile);
        CommandLineParser parser = new BasicParser();
        try {
            homeCmd = parser.parse(options, arguments);
        } catch (ParseException ex) {
            System.err.println("Error occured while parsing '" + propertiesFile.toString() + "' programm options!\n\n");
            throw new RuntimeException(ex);
        }
    }

    public void parse (String[] args) {
        CommandLineParser parser = new BasicParser();
        try {
            runtimeCmd = parser.parse(options, args);
        } catch (ParseException ex) {
            System.err.println("Error occured while parsing programm options!\n\n");
            printUsage();
            System.exit(1);
        }
        if (runtimeCmd.hasOption("load")) {
            Path loadOptions = Paths.get(runtimeCmd.getOptionValue("load"));
            String[] arguments = getStoredOptions(loadOptions);
            try {
                loadCmd = parser.parse(options, arguments);
            } catch (ParseException ex) {
                System.err.println("ERROR OCCURED: while parsing '" + loadOptions.toString() + "' options!\n\n");
                throw new RuntimeException(ex);
            }
        }
    }

    public String getRequestString () {
        String url = getValue("url-pattern");
        if (url == null || url.isEmpty()) {
            throw new RuntimeException("ERROR OCCURED: url-pattern is empty");
        }
        for (String value : getValues("pattern-arguments")) {
            url = url.replaceFirst("\\{\\?\\}", value);
        }
        return url;
    }

    public String getProxyHost () {
        return getValue("proxy-host");
    }

    public Integer getProxyPort () {
        String value = getValue("proxy-port");
        return (value != null)
               ? Integer.parseInt(value)
               : 0;
    }

    public String getProxyUserName () {
        return getValue("proxy-name");
    }

    public String getProxyUserPassword () {
        return getValue("proxy-password");
    }

    public boolean hasProxySettings () {
        return contains("proxy-host");
    }

    public boolean isVerbose () {
        return contains("verbose");
    }

    public boolean shouldSave () {
        return contains("save");
    }

    public boolean shouldPrintHelp () {
        return contains("help");
    }

    public void printUsage () {
        HelpFormatter helpFormatter = new HelpFormatter();
        String header = "Read product IDs from www.ulmart.ru\n\n";
        String footer = "\nPlease report issues at xxlabaza@gmail.com";
        helpFormatter.printHelp("idsearcher", header, options, footer, true);
    }

    public void save () {
        Path storePath = (runtimeCmd.hasOption("save") && runtimeCmd.getOptionValue("save") != null)
                         ? Paths.get(runtimeCmd.getOptionValue("save"))
                         : propertiesFile;
        save(storePath);
    }

    private String getValue (String key) {
        if (runtimeCmd.hasOption(key)) {
            return runtimeCmd.getOptionValue(key);
        }
        if (loadCmd != null && loadCmd.hasOption(key)) {
            return loadCmd.getOptionValue(key);
        }
        return homeCmd.getOptionValue(key);
    }

    private String[] getValues (String key) {
        if (runtimeCmd.hasOption(key)) {
            return runtimeCmd.getOptionValues(key);
        }
        if (loadCmd != null && loadCmd.hasOption(key)) {
            loadCmd.getOptionValues(key);
        }
        return homeCmd.getOptionValues(key);
    }

    private boolean contains (String key) {
        return runtimeCmd.hasOption(key) || (loadCmd != null && loadCmd.hasOption(key)) || homeCmd.hasOption(key);
    }

    private void save (Path file) {
        List<String> lines = new ArrayList<>();
        List<String> systemOptions = Arrays.asList("s", "l", "h", "v");
        for (Option option : runtimeCmd.getOptions()) {
            if (systemOptions.contains(option.getOpt())) {
                continue;
            }

            StringBuilder sb = new StringBuilder();

            if (option.hasLongOpt()) {
                sb.append("--");
                sb.append(option.getLongOpt());
            } else {
                sb.append('-');
                sb.append(option.getOpt());
            }

            if (option.hasArgs() && option.getValues() != null) {
                sb.append(' ');
                for (String value : option.getValues()) {
                    sb.append(value);
                    sb.append(option.getValueSeparator());
                }
                sb.delete(sb.length() - 1, sb.length());
            } else if (option.hasArg() && option.getValue() != null) {
                sb.append(' ');
                sb.append(option.getValue());
            }
            lines.add(sb.toString());
        }
        try {
            Files.write(file, lines, Charset.defaultCharset());
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    private String[] getStoredOptions (Path file) {
        if (!Files.exists(file)) {
            return new String[0];
        }
        try {
            final List<String> result = new ArrayList<>();
            Files.lines(file).forEach(new Consumer<String>() {

                @Override
                public void accept (String line) {
                    result.addAll(Arrays.asList(line.split(" ", 2)));
                }
            });
            return result.toArray(new String[result.size()]);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
}
