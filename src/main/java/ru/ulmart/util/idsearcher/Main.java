package ru.ulmart.util.idsearcher;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.DefaultCredentialsProvider;
import com.gargoylesoftware.htmlunit.IncorrectnessListener;
import com.gargoylesoftware.htmlunit.NicelyResynchronizingAjaxController;
import com.gargoylesoftware.htmlunit.ScriptException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebClientOptions;
import com.gargoylesoftware.htmlunit.html.HTMLParserListener;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.javascript.JavaScriptErrorListener;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import org.apache.commons.logging.LogFactory;
import org.w3c.css.sac.CSSException;
import org.w3c.css.sac.CSSParseException;
import org.w3c.css.sac.ErrorHandler;

/**
 *
 * <p>
 * @author Artem Labazin, xxlabaza@gmail.com
 * <p>
 * @since Aug 20, 2014
 * <p>
 * @version 1.0.0
 */
public class Main {

    private static final String XPATH_PRODUCT_ANCHOR = "//div[@class='b-product-list-item__title']/a";

    private static final String XPATH_SHOW_MORE_BUTTON = "//a[contains(@class, 'js-show-more')]";

    public static void main (String[] args) throws IOException {
        Settings settings = Settings.getInstance(args);
        if (settings.shouldPrintHelp()) {
            settings.printUsage();
            System.exit(0);
        }
        if (settings.shouldSave()) {
            settings.save();
            System.exit(0);
        }

        WebClient webClient = createWebClient();
        System.out.println("Loading page: " + settings.getRequestString() + "\n");
        HtmlPage page = webClient.getPage(settings.getRequestString());

        List<HtmlAnchor> list = processPagination(page);
        if (settings.isVerbose()) {
            for (HtmlAnchor anchor : list) {
                System.out.println(anchor.getHrefAttribute().replace("/goods/", "") + "\t - " +
                                   anchor.getTextContent().trim());
            }
        } else {
            for (HtmlAnchor anchor : list) {
                System.out.println(anchor.getHrefAttribute().replace("/goods/", ""));
            }
        }
    }

    private static List<HtmlAnchor> processPagination (HtmlPage page) throws IOException {
        HtmlAnchor showMoreButton = (HtmlAnchor) page.getFirstByXPath(XPATH_SHOW_MORE_BUTTON);
        if (showMoreButton == null) {
            return Collections.emptyList();
        }
        @SuppressWarnings("unchecked")
        List<HtmlAnchor> result = (List<HtmlAnchor>) page.getByXPath(XPATH_PRODUCT_ANCHOR);
        showMoreButton.click();
        page.getWebClient().waitForBackgroundJavaScript(500);
        page.getWebClient().waitForBackgroundJavaScriptStartingBefore(500);
        return (result.size() != page.getByXPath(XPATH_PRODUCT_ANCHOR).size())
               ? processPagination(page)
               : result;
    }

    private static WebClient createWebClient () {
        LogFactory.getFactory()
                .setAttribute("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.NoOpLog");
        java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(Level.OFF);
        java.util.logging.Logger.getLogger("org.apache.commons.httpclient").setLevel(Level.OFF);

        Settings settings = Settings.getInstance();
        WebClient webClient;
        if (settings.hasProxySettings()) {
            webClient = new WebClient(BrowserVersion.CHROME, settings.getProxyHost(), settings.getProxyPort());
            DefaultCredentialsProvider credentials = (DefaultCredentialsProvider) webClient.getCredentialsProvider();
            credentials.addCredentials(settings.getProxyUserName(), settings.getProxyUserPassword());
        } else {
            webClient = new WebClient();
        }
        webClient.setIncorrectnessListener(new IncorrectnessListener() {

            @Override
            public void notify (String string, Object o) {
                // Nothing to do
            }
        });
        webClient.setAjaxController(new NicelyResynchronizingAjaxController());
        webClient.setCssErrorHandler(new ErrorHandler() {

            @Override
            public void warning (CSSParseException exception) throws CSSException {
                // Nothing to do
            }

            @Override
            public void fatalError (CSSParseException exception) throws CSSException {
                // Nothing to do
            }

            @Override
            public void error (CSSParseException exception) throws CSSException {
                // Nothing to do
            }
        });
        webClient.setJavaScriptErrorListener(new JavaScriptErrorListener() {

            @Override
            public void scriptException (HtmlPage hp, ScriptException se) {
                // Nothing to do
            }

            @Override
            public void timeoutError (HtmlPage hp, long l, long l1) {
                // Nothing to do
            }

            @Override
            public void malformedScriptURL (HtmlPage hp, String string, MalformedURLException murle) {
                // Nothing to do
            }

            @Override
            public void loadScriptError (HtmlPage hp, URL url, Exception excptn) {
                // Nothing to do
            }
        });
        webClient.setHTMLParserListener(new HTMLParserListener() {

            @Override
            public void error (String string, URL url, String string1, int i, int i1, String string2) {
                // Nothing to do
            }

            @Override
            public void warning (String string, URL url, String string1, int i, int i1, String string2) {
                // Nothing to do
            }
        });

        WebClientOptions options = webClient.getOptions();

        options.setPrintContentOnFailingStatusCode(false);
        options.setThrowExceptionOnScriptError(false);
        options.setThrowExceptionOnFailingStatusCode(false);

        return webClient;
    }
}
